﻿using System.Windows;
using ManipulationWithSuggestions;
using ManipulationWithSuggestions.Interfaces;

namespace WordSeparator
{
    public partial class MainWindow : Window
    {
        private readonly ISuggestion _suggestion;

        public MainWindow()
        {
            InitializeComponent();

            _suggestion = new Suggestion();
        }

        private void CopyResultToClipboard_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(_resultTextBox.Text);
        }

        private void PasteFromClipboard1_Click(object sender, RoutedEventArgs e)
        {
            _textSplit.Text = Clipboard.GetText();
        }

        private void PasteFromClipboard2_Click(object sender, RoutedEventArgs e)
        {
            _textConcatenate.Text = Clipboard.GetText();
        }

        private void SplitWords_Click(object sender, RoutedEventArgs e)
        {
            bool toLower = _firstSymbolToLowerChBox.IsChecked.HasValue && _firstSymbolToLowerChBox.IsChecked.Value;
            _resultTextBox.Text = _suggestion.SplitWords(_textSplit.Text, toLower);
        }

        private void ConcatenateWords_Click(object sender, RoutedEventArgs e)
        {
            _resultTextBox.Text = _suggestion.ToConcatenateWords(_textConcatenate.Text,
                (bool)_firstSymbolToUpperChBox.IsChecked,
                (bool)_concatenateLowerCaseWordsChBox.IsChecked);
        }
    }
}
