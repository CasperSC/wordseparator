﻿namespace ManipulationWithSuggestions.Interfaces
{
    public interface ISuggestion
    {
        /// <summary>
        /// Расцепить слова
        /// </summary>
        /// <param name="source"></param>
        /// <param name="symbolsToLowerCase">Задаёт нужно ли переводить символы в нижний регистр</param>
        /// <param name="skipFirstSymbol">Задаёт оставлять ли первый символ в верхнем регистре, 
        /// если задано переводить все символы в нижний регистр</param>
        /// <returns></returns>
        string SplitWords(string source, bool symbolsToLowerCase, bool skipFirstSymbol = true);

        /// <summary>
        /// Сцепить слова
        /// </summary>
        /// <param name="source"></param>
        /// <param name="convertFirstLetterToUpperCase">
        /// Задает нужно ли конвертировать первые символы каждого слова 
        /// в верхний регистр, если оно в нижнем регистре</param>
        /// <param name="concatenateLowerCaseWords">Задаёт сцеплять ли слова начинающиеся с символа в нижнем регистре</param>
        /// <returns></returns>
        string ToConcatenateWords(string source, bool convertFirstLetterToUpperCase, bool concatenateLowerCaseWords);
    }
}
