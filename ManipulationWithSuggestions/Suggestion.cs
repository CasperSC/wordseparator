﻿using System;
using System.Text;
using ManipulationWithSuggestions.Interfaces;

namespace ManipulationWithSuggestions
{
    public class Suggestion : ISuggestion
    {
        /// <summary>
        /// Расцепить слова
        /// </summary>
        /// <param name="source"></param>
        /// <param name="symbolsToLowerCase">Задаёт нужно ли переводить символы в нижний регистр</param>
        /// <param name="skipFirstSymbol">Задаёт оставлять ли первый символ в верхнем регистре, 
        /// если задано переводить все символы в нижний регистр</param>
        /// <returns></returns>
        public string SplitWords(string source, bool symbolsToLowerCase, bool skipFirstSymbol = true)
        {
            var sb = new StringBuilder(source.Length);
            int lastStartIndex = 0;
            for (int i = 0; i < source.Length; i++)
            {
                char ch = source[i];
                if (char.IsLetter(ch) && char.IsUpper(ch))
                {
                    if (sb.Length > 0)
                        sb.Append(" ");

                    string word = source.Substring(lastStartIndex, i - lastStartIndex);

                    if (lastStartIndex == 0 && symbolsToLowerCase && !skipFirstSymbol)
                    {
                        word = word.ToLower();
                    }
                    else if (lastStartIndex != 0 && symbolsToLowerCase)
                    {
                        word = word.ToLower();
                    }

                    sb.Append(word);
                    lastStartIndex = i;
                }
            }

            if (sb.Length > 0)
                sb.Append(" ");

            string word2 = source.Substring(lastStartIndex, source.Length - lastStartIndex);

            if (lastStartIndex == 0 && symbolsToLowerCase && !skipFirstSymbol)
            {
                word2 = word2.ToLower();
            }
            else if (lastStartIndex != 0 && symbolsToLowerCase)
            {
                word2 = word2.ToLower();
            }

            sb.Append(word2);

            return sb.ToString();
        }

        /// <summary>
        /// Сцепить слова
        /// </summary>
        /// <param name="source"></param>
        /// <param name="convertFirstLetterToUpperCase">
        /// Задает нужно ли конвертировать первые символы каждого слова 
        /// в верхний регистр, если оно в нижнем регистре</param>
        /// <param name="concatenateLowerCaseWords">Задаёт сцеплять ли слова начинающиеся с символа в нижнем регистре</param>
        /// <returns></returns>
        public string ToConcatenateWords(string source, bool convertFirstLetterToUpperCase, bool concatenateLowerCaseWords)
        {
            var sb = new StringBuilder(source.Length);

            string[] result = source.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in result)
            {
                string temp = string.Empty;
                if (convertFirstLetterToUpperCase)
                {
                    if (line.Length > 0)
                    {
                        temp = char.ToUpper(line[0]) + line.Substring(1);
                    }
                }
                else
                {
                    temp = line;
                }

                if (concatenateLowerCaseWords)
                {
                    if (sb.Length > 0)
                    {
                        if (char.IsLower(line[0]))
                        {
                            sb.Append(" ");
                        }
                    }
                }
                sb.Append(temp);
            }

            return sb.ToString();
        }
    }
}
